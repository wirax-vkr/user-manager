package com.wirax.vkr.usermanager.service;

import com.wirax.vkr.usermanager.model.User;
import com.wirax.vkr.usermanager.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private static List<User> findAllUsersMock = Arrays.asList(new User("Vasiliy", "Petrov"), new User("Maksim", "Sidorov"));

    @Before
    public void setUp() throws Exception {
        when(userRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);
        doNothing().when(userRepository).deleteById(any());
        when(userRepository.findAll()).thenReturn(findAllUsersMock);
    }

    @Test
    public void returnOK_IfUserCreateUses() {
        User creatingUser = new User("Vasily", "Petrov");

        User userVasilyPetrov = userService.create(creatingUser);

        assertEquals(userVasilyPetrov, creatingUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void returnException_whenUpdateIds_notEquals() {
        User creatingUser = new User(UUID.randomUUID(), "Vasily", "Petrov");

        userService.update(UUID.randomUUID(), creatingUser);
    }

    @Test
    public void returnNothing_whenUserDelete_methodUsed() {
        userService.delete(UUID.randomUUID());
    }

    @Test
    public void returnListOfPrecreatedUsers_whenMethodFindAll_methodUsed() {
        List<User> actualResult = userService.findAll();

        assertEquals(findAllUsersMock.size(), actualResult.size());
        assertEquals(findAllUsersMock, actualResult);
    }
}
