package com.wirax.vkr.usermanager.service;

import com.wirax.vkr.usermanager.model.TechnicalSpecialist;
import com.wirax.vkr.usermanager.repository.TechnicalSpecialistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TechnicalSpecialistService {
    private final TechnicalSpecialistRepository technicalSpecialistRepository;

    @Autowired
    public TechnicalSpecialistService(TechnicalSpecialistRepository technicalSpecialistRepository) {
        this.technicalSpecialistRepository = technicalSpecialistRepository;
    }

    public TechnicalSpecialist create(TechnicalSpecialist technicalSpecialist) {
        return technicalSpecialistRepository.save(technicalSpecialist);
    }

    public TechnicalSpecialist update(UUID id, TechnicalSpecialist user) {
        if(id != user.getId()) {
            throw new IllegalArgumentException("Wrong parameter id");
        }
        return technicalSpecialistRepository.save(user);
    }

    public List<TechnicalSpecialist> findAll() {
        return technicalSpecialistRepository.findAll();
    }

    public void delete(UUID id) {
        technicalSpecialistRepository.deleteById(id);
    }
}
