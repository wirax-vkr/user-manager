package com.wirax.vkr.usermanager.repository;

import com.wirax.vkr.usermanager.model.TechnicalSpecialist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TechnicalSpecialistRepository  extends JpaRepository<TechnicalSpecialist, UUID> {
}
