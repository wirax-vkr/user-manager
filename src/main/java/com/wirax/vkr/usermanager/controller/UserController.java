package com.wirax.vkr.usermanager.controller;

import com.wirax.vkr.usermanager.mapper.UserMapper;
import com.wirax.vkr.usermanager.mapper.UserMapperImpl;
import com.wirax.vkr.usermanager.model.dto.UserDto;
import com.wirax.vkr.usermanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper = new UserMapperImpl();

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user")
    public UserDto create(@RequestBody UserDto user) {
        return userMapper.toDto(userService.create(userMapper.toEntity(user)));
    }

    @PutMapping("/user")
    public UserDto update(@RequestParam UUID id, @RequestBody UserDto user) {
        return userMapper.toDto(userService.update(id, userMapper.toEntity(user)));
    }

    @GetMapping("/user")
    public List<UserDto> findAll() {
        return userService.findAll()
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/user")
    public void delete(@RequestParam UUID id) {
        userService.delete(id);
    }
}
