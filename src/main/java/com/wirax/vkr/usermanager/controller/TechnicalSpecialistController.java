package com.wirax.vkr.usermanager.controller;

import com.wirax.vkr.usermanager.mapper.TechnicalSpecialistMapper;
import com.wirax.vkr.usermanager.mapper.TechnicalSpecialistMapperImpl;
import com.wirax.vkr.usermanager.model.TechnicalSpecialist;
import com.wirax.vkr.usermanager.model.dto.TechnicalSpecialistDto;
import com.wirax.vkr.usermanager.service.TechnicalSpecialistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class TechnicalSpecialistController {
    public final TechnicalSpecialistService service;
    private TechnicalSpecialistMapper mapper = new TechnicalSpecialistMapperImpl();

    @Autowired
    public TechnicalSpecialistController(TechnicalSpecialistService service) {
        this.service = service;
    }

    @PostMapping("/technicalSpecialist")
    public TechnicalSpecialist create(@RequestBody TechnicalSpecialistDto dto) {
        return service.create(mapper.toEntity(dto));
    }

    @PutMapping("/technicalSpecialist")
    public TechnicalSpecialist update(@RequestParam UUID id, @RequestBody TechnicalSpecialistDto dto) {
        return service.update(id, mapper.toEntity(dto));
    }

    @GetMapping("/technicalSpecialist")
    public List<TechnicalSpecialist> findAll() {
        return service.findAll();
    }

    @DeleteMapping("/technicalSpecialist")
    public void delete(@RequestParam UUID id) {
        service.delete(id);
    }
}
