package com.wirax.vkr.usermanager.mapper;

import com.wirax.vkr.usermanager.model.User;
import com.wirax.vkr.usermanager.model.dto.UserDto;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {
    User toEntity(UserDto dto);

    UserDto toDto(User entity);
}
