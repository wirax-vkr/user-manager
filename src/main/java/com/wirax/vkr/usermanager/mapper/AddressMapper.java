package com.wirax.vkr.usermanager.mapper;

import com.wirax.vkr.usermanager.model.Address;
import com.wirax.vkr.usermanager.model.dto.AddressDto;
import org.mapstruct.Mapper;

@Mapper
public interface AddressMapper {
    Address toEntity(AddressDto dto);

    AddressDto toDto(Address entity);
}
