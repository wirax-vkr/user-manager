package com.wirax.vkr.usermanager.mapper;

import com.wirax.vkr.usermanager.model.TechnicalSpecialist;
import com.wirax.vkr.usermanager.model.dto.TechnicalSpecialistDto;
import org.mapstruct.Mapper;

@Mapper
public interface TechnicalSpecialistMapper {
    TechnicalSpecialist toEntity(TechnicalSpecialistDto dto);

    TechnicalSpecialistDto toDto(TechnicalSpecialist entity);
}
