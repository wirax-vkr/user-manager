package com.wirax.vkr.usermanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class TechnicalSpecialist {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private Position position;

    private Role role;

    private String experience;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Skill> skills;

    @OneToOne
    private User user;

    public enum Position {
        JUNIOR, MIDDLE, SENIOR
    }

    public enum Role {
        TECHNICAL_MANAGER, SOFTWARE_ENGINEER, BUSINESS_ANALYST, QUALITY_SPECIALIST
    }
}
