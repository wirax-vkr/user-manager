package com.wirax.vkr.usermanager.model.dto;

import com.wirax.vkr.usermanager.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {
    private UUID id;
    private User user;
    private String city;
    private String street;
    private String country;
}
