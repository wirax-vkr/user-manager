package com.wirax.vkr.usermanager.model.dto;

import com.wirax.vkr.usermanager.model.TechnicalSpecialist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TechnicalSpecialistDto {
    private UUID id;
    private TechnicalSpecialist.Position position;
    private TechnicalSpecialist.Role role;
    private String experience;
    private List<SkillDto> skills;
    private UserDto user;
}
