package com.wirax.vkr.usermanager.model.dto;

import com.wirax.vkr.usermanager.model.Skill;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkillDto {
    private UUID id;
    private String name;
    private Skill.Level level;
    private String technology;
}
